package com.epam.rd.java.basic.task8;

public class GrowingTips {
    String tempreture;
    String lighting;
    String  watering;
    String lightRequiring;
    String tempretureMeasure;
    String wateringMeasure;

    public void setTempreture(String tempreture) {
        this.tempreture = tempreture;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public void setWatering(String watering) {
        this.watering = watering;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public void setTempretureMeasure(String tempretureMeasure) {
        this.tempretureMeasure = tempretureMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    public String getTempreture() {
        return tempreture;
    }

    public String getLighting() {
        return lighting;
    }

    public String getWatering() {
        return watering;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public String getTempretureMeasure() {
        return tempretureMeasure;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture='" + tempreture + '\'' +
                ", lighting='" + lighting + '\'' +
                ", watering='" + watering + '\'' +
                ", lightRequiring='" + lightRequiring + '\'' +
                ", tempretureMeasure='" + tempretureMeasure + '\'' +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                '}';
    }
}
