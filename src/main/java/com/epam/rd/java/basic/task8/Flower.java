package com.epam.rd.java.basic.task8;

public class Flower implements Comparable<Flower>{
    String name;
    String soil;
    String origin;
    String multiplying;
    VisualParameters visualParameters;
    GrowingTips growingTips;

    public Flower(String name, String soil, String origin, String multiplying, VisualParameters visualParameters, GrowingTips growingTips) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.multiplying = multiplying;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", multiplying='" + multiplying + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                '}';
    }

    @Override
    public int compareTo(Flower o) {
        if(name == null || o.name == null){
            return 0;
        }
        return name.compareTo(o.name);
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }
}
