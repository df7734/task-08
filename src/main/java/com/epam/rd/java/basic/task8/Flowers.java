package com.epam.rd.java.basic.task8;

import java.util.List;

public class Flowers {
    private List<Flower> flowers;

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    @Override
    public String toString() {
        return flowers.toString();
    }
}
