package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.Flowers;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private Flowers flowers;
	Document doc;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		flowers = new Flowers();

		try {
			File inputFile = new File(xmlFileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}
	}
	public void parse(){
		Node flowersNode = doc.getFirstChild();
		NodeList mainChilds = flowersNode.getChildNodes();
		List<Flower> flowerList = new ArrayList<Flower>();
		for(int i = 0; i< mainChilds.getLength(); i++){
			if(!mainChilds.item(i).getNodeName().equals("flower"))
			{
				continue;
			}
			NodeList flowerChilds = mainChilds.item(i).getChildNodes();

			String name = "";
			String soil = "";
			String origin = "";
			String multiplying = "";
			VisualParameters visualParameters = new VisualParameters();
			GrowingTips growingTips = new GrowingTips();

			for(int j = 0; j < flowerChilds.getLength(); j++) {
				if(flowerChilds.item(j).getNodeType()!= Node.ELEMENT_NODE)
				{
					continue;
				}

				switch (flowerChilds.item(j).getNodeName()){
					case "name" : {
						name = flowerChilds.item(j).getTextContent();
						break;
					}
					case "soil" : {
						soil = flowerChilds.item(j).getTextContent();
						break;
					}
					case "origin" : {
						origin = flowerChilds.item(j).getTextContent();
						break;
					}
					case "multiplying" : {
						multiplying = flowerChilds.item(j).getTextContent();
						break;
					}
					case "visualParameters" : {
						NodeList params = flowerChilds.item(j).getChildNodes();
						for(int k = 0; k < params.getLength(); k++) {
							if (params.item(k).getNodeType() != Node.ELEMENT_NODE) {
								continue;
							}
							switch (params.item(k).getNodeName()) {
								case "stemColour": {
									visualParameters.setStemColour(params.item(k).getTextContent());
									break;
								}
								case "leafColour": {
									visualParameters.setLeafColour(params.item(k).getTextContent());
									break;
								}
								case "aveLenFlower": {
									visualParameters.setMeasure(params.item(k).getAttributes().item(0).getTextContent());
									visualParameters.setAveLenFlower(params.item(k).getTextContent());
									break;
								}
							}
						}
							break;
					}
					case "growingTips" : {
						NodeList tips = flowerChilds.item(j).getChildNodes();
						for(int k = 0; k < tips.getLength(); k++) {
							if (tips.item(k).getNodeType() != Node.ELEMENT_NODE) {
								continue;
							}
							switch (tips.item(k).getNodeName()) {
								case "tempreture": {
									growingTips.setTempretureMeasure(tips.item(k).getAttributes().item(0).getTextContent());
									growingTips.setTempreture(tips.item(k).getTextContent());
									break;
								}
								case "lighting": {
									growingTips.setLightRequiring(tips.item(k).getAttributes().item(0).getTextContent());
									growingTips.setLighting(tips.item(k).getTextContent());
									break;
								}
								case "watering": {
									growingTips.setWateringMeasure(tips.item(k).getAttributes().item(0).getTextContent());
									growingTips.setWatering(tips.item(k).getTextContent());
									break;
								}
							}
						}
						break;
					}

				}
			}

			Flower flower = new Flower(name, soil, origin, multiplying, visualParameters, growingTips);
			flowerList.add(flower);
		}

			flowers.setFlowers(flowerList);

	}

	public void sort(){
		Collections.sort(flowers.getFlowers());
	}


	/*
	Collections.sort(users, new Comparator<User>() {
  @Override
  public int compare(User u1, User u2) {
    return u1.getCreatedOn().compareTo(u2.getCreatedOn());
  }
});
	 */


	public void save(String path){

		File f = new File(path);

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();

			Element rootElement = doc.createElement("flowers");
			rootElement.setAttribute("xmlns", "http://www.nure.ua");
			rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
			doc.appendChild(rootElement);

			for(int i=0; i<flowers.getFlowers().size(); i++){
				Element flower = doc.createElement("flower");
				// add staff to root
				rootElement.appendChild(flower);


				Element name = doc.createElement("name");
				name.setTextContent(flowers.getFlowers().get(i).getName());
				flower.appendChild(name);

				Element soil = doc.createElement("soil");
				soil.setTextContent(flowers.getFlowers().get(i).getSoil());
				flower.appendChild(soil);

				Element origin = doc.createElement("origin");
				origin.setTextContent(flowers.getFlowers().get(i).getOrigin());
				flower.appendChild(origin);

				//visual params
				Element visualParameters = doc.createElement("visualParameters");
				Element stemColour = doc.createElement("stemColour");
				stemColour.setTextContent(flowers.getFlowers().get(i).getVisualParameters().getStemColour());
				visualParameters.appendChild(stemColour);
				Element leafColour = doc.createElement("leafColour");
				leafColour.setTextContent(flowers.getFlowers().get(i).getVisualParameters().getLeafColour());
				visualParameters.appendChild(leafColour);
				Element aveLenFlower = doc.createElement("aveLenFlower");
				aveLenFlower.setAttribute("measure", flowers.getFlowers().get(i).getVisualParameters().getMeasure());
				aveLenFlower.setTextContent(flowers.getFlowers().get(i).getVisualParameters().getAveLenFlower());
				visualParameters.appendChild(aveLenFlower);
				flower.appendChild(visualParameters);


				//tips
				Element growingTips = doc.createElement("growingTips");

				Element tempreture = doc.createElement("tempreture");
				tempreture.setAttribute("measure", flowers.getFlowers().get(i).getGrowingTips().getTempretureMeasure());
				tempreture.setTextContent(flowers.getFlowers().get(i).getGrowingTips().getTempreture());
				growingTips.appendChild(tempreture);


				Element lighting = doc.createElement("lighting");
				lighting.setAttribute("lightRequiring", flowers.getFlowers().get(i).getGrowingTips().getLightRequiring());
				growingTips.appendChild(lighting);

				Element watering = doc.createElement("watering");
				watering.setAttribute("measure", flowers.getFlowers().get(i).getGrowingTips().getWateringMeasure());
				watering.setTextContent(flowers.getFlowers().get(i).getGrowingTips().getWatering());
				growingTips.appendChild(watering);

				flower.appendChild(growingTips);

				//
				Element multiplying = doc.createElement("multiplying");
				multiplying.setTextContent(flowers.getFlowers().get(i).getMultiplying());
				flower.appendChild(multiplying);
			}

			try (FileOutputStream output =
						 new FileOutputStream(path)) {
				writeXml(doc, output);
			} catch (IOException | TransformerException e) {
				e.printStackTrace();
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}


	}

	private static void writeXml(Document doc, OutputStream output) throws TransformerException {

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		doc.setXmlStandalone(true);

		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result);

	}

}
