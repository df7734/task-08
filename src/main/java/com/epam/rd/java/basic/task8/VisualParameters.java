package com.epam.rd.java.basic.task8;

public class VisualParameters {
    String stemColour;
    String leafColour;
    String  aveLenFlower;

    String measure;


    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(String  aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public String getAveLenFlower() {
        return aveLenFlower;
    }

    public String getMeasure() {
        return measure;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower='" + aveLenFlower + '\'' +
                ", measure='" + measure + '\'' +
                '}';
    }
}
